package com.arquigames.slidingpuzzle.utils.math;

public class Vector2d{
    public double x,y;
    public Vector2d(){

    }
    public Vector2d(double x,double y){
        this.x = x;
        this.y = y;
    }
    public double sqrt2(){
        return x*x +y*y;
    }
    public double length(){
        return Math.sqrt(this.sqrt2());
    }
    public Vector2d normalize(){
        double length = length();
        x /= length;
        y /= length;
        return this;
    }
    public Vector2d normalize(Vector2d target){
        return copy(target).normalize();
    }
    public Vector2d clone(){
        Vector2d target = new Vector2d();
        return this.copy(target);
    }
    public Vector2d copy(Vector2d target){
        target.x = x;
        target.y = y;
        return target;
    }
    public Vector2d sub(Vector2d v){
        this.x -= v.x;
        this.y -= v.y;
        return this;
    }

    public Vector2d add(Vector2d v) {
        this.x += v.x;
        this.y += v.y;
        return this;
    }

    public Vector2d maxLength(int size) {
        if(length()>size) return this.normalize().multiplyScalar(size);
        return this;
    }
    public Vector2d multiplyScalar(int scalar){
        this.x *=scalar;
        this.y *=scalar;
        return this;
    }
}