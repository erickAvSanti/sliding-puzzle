package com.arquigames.slidingpuzzle.drawables;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import java.io.File;

public class CustomDrawable {
    public Drawable drawable;
    public File origin;
    public CustomDrawable(Drawable drawable,File origin){
        this.drawable = drawable;
        this.origin = origin;
    }
}
