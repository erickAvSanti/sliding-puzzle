package com.arquigames.slidingpuzzle.tasks;

import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.util.Log;

import com.arquigames.slidingpuzzle.drawables.CustomDrawable;

import java.io.File;

public class LoadBitmapTask extends AsyncTask<File, CustomDrawable, Void> {
    private static final String TAG = "LoadBitmapTask";
    private CallBackResult cb;

    public LoadBitmapTask(CallBackResult cb){
        this.cb = cb;
    }
    @Override
    protected Void doInBackground(File... files) {
        Log.e(TAG,"doInBackground");
        for(File file : files){
            Log.e(TAG,"Process file "+file.getAbsolutePath());
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile( file.getAbsolutePath(), options);
            if( options.outWidth == options.outHeight && options.outHeight > 0 ){
                CustomDrawable cd = new CustomDrawable(BitmapDrawable.createFromPath( file.getAbsolutePath() ),file);
                if(isCancelled())break;
                publishProgress(cd);
            }
        }
        return null;
    }
    protected void onProgressUpdate(CustomDrawable... bitmap){
        if(cb != null){
            cb.putDrawable(bitmap[0]);
        }
    }
    protected void onPostExecute(Void empty){
        if(cb != null){
            cb.drawablesLoaded();
        }
    }
    public interface CallBackResult{
        void putDrawable(CustomDrawable drawable);
        void drawablesLoaded();
    }
}
