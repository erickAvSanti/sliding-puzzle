package com.arquigames.slidingpuzzle.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.arquigames.slidingpuzzle.swipes.PuzzleSwipeManager;
import com.arquigames.slidingpuzzle.swipes.PuzzleSwipeManager2;

import java.io.File;

public class PuzzleView extends View {
    private static final String TAG = "PuzzleView";
    private File image_src;
    private Paint linePaint = new Paint();
    private Paint boxPaint = new Paint();
    private Bitmap image;
    Rect rect_img_src = new Rect();
    Rect rect_img_dst = new Rect();
    private Paint img_paint = new Paint();

    PuzzleSwipeManager2 swipeManager = null;


    public PuzzleView(Context context) {
        super(context);
    }

    public PuzzleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public PuzzleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public PuzzleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(swipeManager == null){
            swipeManager = new PuzzleSwipeManager2(this,getMeasuredWidth(),getMeasuredHeight(),image);
            linePaint.setAlpha(40);
            linePaint.setColor(Color.RED);
            linePaint.setStyle(Paint.Style.FILL);

            boxPaint.setColor(Color.BLACK);
            boxPaint.setStyle(Paint.Style.FILL);
        }
        int ww = getMeasuredWidth();
        int hh = getMeasuredHeight();
        canvas.drawRect(0,0,ww,hh,boxPaint);
        canvas.drawCircle(ww / 2, hh / 2, 200, linePaint);
        /*
        if(image_src != null){
            if( image != null ){

                int size = ww > hh ? hh : ww;
                int step = size / 4;
                rect_img_dst.left = ( ww - size ) / 2;
                rect_img_dst.top = ( hh - size ) / 2;
                rect_img_dst.right = size + rect_img_dst.left;
                rect_img_dst.bottom = size + rect_img_dst.top;

                canvas.drawBitmap(image,rect_img_src,rect_img_dst,img_paint);

                for(int i = 0 ; i < 5 ; i++ ){
                    canvas.drawLine(i*step,rect_img_dst.top, i*step, rect_img_dst.bottom, linePaint);
                }
                for(int i = 0 ; i < 5 ; i++ ){
                    canvas.drawLine(rect_img_dst.left,rect_img_dst.top + i*step, rect_img_dst.right, rect_img_dst.top + i*step, linePaint);
                }

            }
        }
        */
        if(swipeManager!=null){
            swipeManager.draw(canvas);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public File getImage_src() {
        return image_src;
    }

    public void setImage_src(File image_src) {
        this.image_src = image_src;
        image = BitmapFactory.decodeFile(image_src.getAbsolutePath());

        rect_img_src.left = 0;
        rect_img_src.top = 0;
        rect_img_src.right = image.getWidth();
        rect_img_src.bottom = image.getHeight();
    }
    public void reDraw(){
        this.invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getActionMasked();
        Log.e(TAG,"onTouchEvent, action = "+action+", actionIndex = "+event.getActionIndex());
        if(action == MotionEvent.ACTION_DOWN){
            onTouchStart(event);
        }else if(action == MotionEvent.ACTION_MOVE){
            onTouchMove(event);
        }else if(action == MotionEvent.ACTION_UP){
            onTouchEnd(event);
        }else if(action == MotionEvent.ACTION_CANCEL){
            onTouchEnd(event);
        }else if(action == MotionEvent.ACTION_OUTSIDE){
            onTouchEnd(event);
        }else{
            return super.onTouchEvent(event);
        }
        return true;
    }

    private void onTouchStart(MotionEvent event) {
        //reDraw();
    }
    private void onTouchMove(MotionEvent event) {
        //reDraw();
    }
    private void onTouchEnd(MotionEvent event) {
        if(event.getActionIndex() == 0){
            if(swipeManager!=null){
                swipeManager.onTouchEnd(event.getX(0),event.getY(0));
            }
        }
        reDraw();
    }

}
