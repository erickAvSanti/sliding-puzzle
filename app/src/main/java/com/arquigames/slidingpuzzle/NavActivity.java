package com.arquigames.slidingpuzzle;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;

import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.arquigames.slidingpuzzle.adapters.SimpleItemRecyclerViewAdapter;
import com.arquigames.slidingpuzzle.dialogs.RemoveImagePuzzleDialog;
import com.arquigames.slidingpuzzle.drawables.CustomDrawable;
import com.arquigames.slidingpuzzle.swipes.SwipeController2;
import com.arquigames.slidingpuzzle.swipes.SwipeControllerActions;
import com.arquigames.slidingpuzzle.tasks.LoadBitmapTask;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.Menu;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class NavActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, LoadBitmapTask.CallBackResult, RemoveImagePuzzleDialog.NoticeDialogListener {

    private static final String TAG = "NavActivity";
    private static final String REMOVE_IMAGE_PUZZLE_DIALOG = "remove_image_puzzle";

    private DrawerLayout drawer;


    String currentPhotoPath;
    static final int REQUEST_TAKE_PHOTO = 1;
    static final int PICK_IMAGE_REQUEST_CODE = 2;
    static final int READ_EXTERNAL_STORAGE_REQUEST_CODE = 3;

    private RecyclerView recyclerView;

    private Thread threadDrawables;
    private File currentPhotoFile;

    private Paint paintSwipped = new Paint();

    private LoadBitmapTask loadBitmapTask;

    private SwipeRefreshLayout swipeRefreshLayout;
    private SwipeController2 swipeController;
    private InterstitialAd mInterstitialAd;
    private Intent mCurrentIntentToLoad;

    public static boolean WITH_AD_BANNER = true;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_nav );
        Toolbar toolbar = findViewById( R.id.toolbar );
        setSupportActionBar( toolbar );

        FloatingActionButton add_from_gallery = findViewById(R.id.add_from_gallery);
        add_from_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImageFromGallery(null);
            }
        });

        FloatingActionButton add_from_camera = findViewById(R.id.add_from_camera);
        add_from_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture2(null);
            }
        });

        drawer = findViewById( R.id.drawer_layout );
        NavigationView navigationView = findViewById( R.id.nav_view );
        navigationView.setNavigationItemSelectedListener( this );

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        /*
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,
                R.id.nav_tools, R.id.nav_share, R.id.nav_send)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        */


        MobileAds.initialize(this, getResources().getString(R.string.ad_application_id));
        AdView mAdView = findViewById(R.id.adView);
        if(WITH_AD_BANNER){
            mAdView.loadAd( new AdRequest.Builder().build() );
        }else{
            mAdView.setVisibility(View.GONE);
        }

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.ad_interstitial_1));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the interstitial ad is closed.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
                if(mCurrentIntentToLoad != null){
                    NavActivity.this.startActivity(mCurrentIntentToLoad);
                }
            }
        });

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Esto se ejecuta cada vez que se realiza el gesto
                processDrawables2();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.color_white);
        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.color_soft_1);


        recyclerView = drawer.findViewById(R.id.image_list);
        swipeController = new SwipeController2(new SwipeControllerActions() {
            @Override
            public void onLeftClicked(int position) {
                new RemoveImagePuzzleDialog(position).show(getSupportFragmentManager(),REMOVE_IMAGE_PUZZLE_DIALOG);
            }
            @Override
            public void onRightClicked(int position) {
                new RemoveImagePuzzleDialog(position).show(getSupportFragmentManager(),REMOVE_IMAGE_PUZZLE_DIALOG);
            }
        });
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeController);
        itemTouchHelper.attachToRecyclerView(recyclerView);
        this.processDrawables2();
    }
    public void reloadImages(View view){
        this.processDrawables2(true);
    }
    private void processDrawables2(){
        this.processDrawables2(false);
    }
    private void processDrawables2(boolean forceReload){
        swipeRefreshLayout.setRefreshing(false);
        SimpleItemRecyclerViewAdapter adapter = ((SimpleItemRecyclerViewAdapter)recyclerView.getAdapter());
        ArrayList<File> otherFiles = null;
        if(adapter!=null){
            if(forceReload){
                adapter.mValues.clear();
                adapter.notifyDataSetChanged();
            }else{
                otherFiles = adapter.getDrawablesFiles();
            }
        }
        if(loadBitmapTask != null)loadBitmapTask.cancel(true);
        loadBitmapTask = new LoadBitmapTask(this);
        loadBitmapTask.execute(getDrawablesFiles(otherFiles));
    }
    private void processDrawables(){
        if( threadDrawables != null ) threadDrawables.interrupt();
        ArrayList<File> drawableFiles = null;
        SimpleItemRecyclerViewAdapter adapter = ((SimpleItemRecyclerViewAdapter)recyclerView.getAdapter());
        if (adapter != null ){
            drawableFiles = adapter.getDrawablesFiles();
        }
        final ArrayList<File> finalDrawableFiles1 = drawableFiles;
        final Handler handler = new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage(@NonNull Message msg) {
                if(msg.what==0){
                    DrawableTask drawableTask = (DrawableTask)msg.obj;
                    Log.e(TAG,"SETTING ADAPTER FROM HANDLER");
                    SimpleItemRecyclerViewAdapter adapter = ((SimpleItemRecyclerViewAdapter)recyclerView.getAdapter());
                    if( adapter != null ){
                        if( drawableTask.values.size() == 0 ){
                            if( finalDrawableFiles1 == null ){
                                adapter.mValues.clear();
                                adapter.notifyDataSetChanged();
                            }
                        }else{
                            for(CustomDrawable cd : drawableTask.values){
                                adapter.mValues.add(cd);
                                adapter.notifyItemInserted(adapter.mValues.size());
                            }
                        }
                    }else{
                        if(drawableTask.values.size()>0)recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(NavActivity.this,drawableTask.values));
                    }
                }else{
                    super.handleMessage(msg);
                }
            }
        };
        Log.e(TAG,"processing drawables");
        final ArrayList<File> finalDrawableFiles = drawableFiles;
        threadDrawables = new Thread(new Runnable() {
            @Override
            public void run() {
                Thread thread = threadDrawables;
                DrawableTask dt = new DrawableTask(NavActivity.this.getDrawables(thread, finalDrawableFiles));
                if(!thread.isInterrupted())handler.obtainMessage(0,dt).sendToTarget();
            }
        });
        threadDrawables.start();
    }

    @Override
    public void onDialogPositiveClickRemoveImagePuzzle(RemoveImagePuzzleDialog dialog,int position) {
        SimpleItemRecyclerViewAdapter adapter = (SimpleItemRecyclerViewAdapter) recyclerView.getAdapter();
        if(adapter != null){
            CustomDrawable cd = adapter.mValues.get(position);
            if(cd.origin.delete()){
                adapter.mValues.remove(position);
                adapter.notifyItemRemoved(position);
                adapter.notifyItemRangeChanged(position, adapter.mValues.size());
            }
        }
    }

    @Override
    public void onDialogNegativeClickRemoveImagePuzzle(RemoveImagePuzzleDialog dialog,int position) {
        //TODO
    }

    private class DrawableTask{
        ArrayList<CustomDrawable> values;
        DrawableTask(ArrayList<CustomDrawable> values){
            this.values = values;
        }
    }


    @Override
    public void putDrawable(CustomDrawable drawable) {
        SimpleItemRecyclerViewAdapter adapter = ((SimpleItemRecyclerViewAdapter)recyclerView.getAdapter());
        if(adapter != null){
            adapter.mValues.add(drawable);
            adapter.notifyItemInserted(adapter.mValues.size());
        }else{
            ArrayList<CustomDrawable> values = new ArrayList<>();
            values.add(drawable);
            recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(NavActivity.this,values));
        }
    }

    @Override
    public void drawablesLoaded() {
        swipeRefreshLayout.setRefreshing(false);
    }

    private boolean fileExistsInList(File file, ArrayList<File> files){
        if(files == null || files.size() == 0)return false;
        for(File f : files){
            if(file.getAbsolutePath().equals(f.getAbsolutePath())){
                return true;
            }
        }
        return false;
    }
    private File[] getDrawablesFiles(ArrayList<File> otherFiles){
        ArrayList<File> arr = new ArrayList<>();
        File dir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (dir != null) {
            for( File file : Objects.requireNonNull(dir.listFiles())){
                if(file.isFile() && this.isJpgOrPngFile(file) && !fileExistsInList(file,otherFiles)){
                    arr.add(file);
                }
            }
        }
        Log.e(TAG,"Total getDrawablesFiles = "+arr.size());
        File[] files = new File[arr.size()];
        int count = 0;
        for(File f : arr){
            files[count] = f;
            count ++;
        }
        return files;
    }
    private ArrayList<CustomDrawable> getDrawables(Thread thread, ArrayList<File> finalDrawableFiles){
        ArrayList<CustomDrawable> arr = new ArrayList<>();
        File[] drawablesFiles = getDrawablesFiles(finalDrawableFiles);
        for(File file: drawablesFiles){

            if(thread != null && thread.isInterrupted()){
                arr.clear();
                return arr;
            }

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile( file.getAbsolutePath(), options);
            if( options.outWidth == options.outHeight && options.outHeight > 0 ){
                CustomDrawable cd = new CustomDrawable(BitmapDrawable.createFromPath( file.getAbsolutePath() ),file);
                arr.add( cd );
            }
        }
        return arr;
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.nav, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        boolean pass = false;
        if(menuItem.getItemId() == R.id.nav_load_from_library){
            pickImageFromGallery(null);
        }
        if(menuItem.getItemId() == R.id.nav_take_a_picture){
            takePicture2(null);
        }
        drawer.closeDrawers();
        return pass;
    }

    public void takePicture2(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        BuildConfig.APPLICATION_ID,
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if( resultCode == RESULT_OK ){
            boolean reload_drawables = false;
            if (REQUEST_TAKE_PHOTO == requestCode) {
                this.galleryAddPic();
                if( data!=null ){
                    reload_drawables = this.parseUriImage(data);
                }
                else{
                    convertImageFileToSquareSize(currentPhotoFile,null);
                    reload_drawables = true;
                }
            }
            if (requestCode == PICK_IMAGE_REQUEST_CODE ){
                if( data!=null ){
                    reload_drawables = this.parseUriImage(data);
                }
            }
            if(reload_drawables)this.processDrawables2();
        }
        super.onActivityResult(requestCode,resultCode,data);
    }
    private boolean parseUriImage(Intent data){

        Log.e(TAG," parseUriImage ");
        Uri uri = data.getData();
        if (uri != null) {
            File src = uriToImageFile(uri);
            if(src != null && isJpgOrPngFile(src)){
                try {
                    File dst = createImageFile();
                    Log.e(TAG,"src = "+src.getAbsolutePath());
                    Log.e(TAG,"dst = "+dst.getAbsolutePath());
                    convertImageFileToSquareSize(src,dst);
                    return true;
                } catch (IOException e) {
                    printStackTrace(e);
                }
            }else{
                Log.e(TAG,"cant get file path from uri");
            }
        }
        return false;
    }

    private File uriToImageFile(Uri uri){
        String[] filePathColumn = new String[]{MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri,filePathColumn,null,null,null);
        if(cursor!=null){
            if(cursor.moveToFirst()){
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();
                return new File(filePath);
            }
        }
        return null;
    }
    private boolean isJpgOrPngFile(File file){
        return file.getName().toLowerCase().endsWith("jpeg") || file.getName().toLowerCase().endsWith("jpg") || file.getName().toLowerCase().endsWith("png");
    }
    private void convertImageFileToSquareSize(File file, File dst) {
        if(isJpgOrPngFile(file)){

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(file.getAbsolutePath(), options);
            int imageHeight = options.outHeight;
            int imageWidth = options.outWidth;
            Log.e(TAG,"FILE IMAGE "+file.getAbsolutePath()+", width: "+imageWidth+", height: "+imageHeight);

            Log.e(TAG,"CROPPING IMAGE "+file.getAbsolutePath());
            this.cropImage(file,imageWidth,imageHeight,dst);

        }
    }
    private void cropImage(File imageFile,int width, int height, File dst){
        if(dst == null) dst = imageFile;
        try {
            Bitmap source = BitmapFactory.decodeStream(new FileInputStream(imageFile));
            int size = width > height ? height : width;
            int left,top;
            if( width > height){
                left = ( width - height ) / 2;
                top = 0;
            }else{
                left = 0;
                top = ( height - width ) / 2;
            }
            int dstSize = size > 2000 ? 2000 : size;
            Paint paint = new Paint();
            Bitmap target = Bitmap.createBitmap(dstSize,dstSize,Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(target);
            Rect rect_src = new Rect(left,top,size + left,size + top);
            Rect rect_dst = new Rect(0,0,dstSize,dstSize);
            canvas.drawBitmap(source,rect_src,rect_dst,paint);
            target.compress(Bitmap.CompressFormat.JPEG,100,new FileOutputStream(dst));

        } catch (Exception e) {
            NavActivity.printStackTrace(e);
        }
    }
    public static void printStackTrace(Exception e){
        for( StackTraceElement t : e.getStackTrace() ){
            Log.e(TAG,t.toString());
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoFile = image;
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }
    private void galleryAddPic() {
        if(currentPhotoPath!=null && currentPhotoPath.length()>0){
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            File f = new File(currentPhotoPath);
            Uri contentUri = Uri.fromFile(f);
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);
        }
    }
    public void pickImageFromGallery(View view) {
        if( ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED ){
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_MIME_TYPES,new String[]{"image/png","image/jpeg"});
            startActivityForResult(intent,PICK_IMAGE_REQUEST_CODE);
        }else{
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},READ_EXTERNAL_STORAGE_REQUEST_CODE);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == READ_EXTERNAL_STORAGE_REQUEST_CODE){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                pickImageFromGallery(null);
            }
        }
    }

    public void showAd(Intent intent) {
        mCurrentIntentToLoad = intent;
        counter_show_ad++;
        if (mInterstitialAd.isLoaded() && counter_show_ad % 2 == 0) {
            mInterstitialAd.show();
        } else {
            startActivity(intent);
        }
    }
    private int counter_show_ad = 0;
}
