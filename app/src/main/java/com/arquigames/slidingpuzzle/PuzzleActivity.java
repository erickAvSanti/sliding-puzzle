package com.arquigames.slidingpuzzle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.arquigames.slidingpuzzle.views.PuzzleSurfaceView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.util.Objects;

import static com.arquigames.slidingpuzzle.NavActivity.WITH_AD_BANNER;

public class PuzzleActivity extends AppCompatActivity {
    public static String IMAGE_FILE_PUZZLE = "image_file_puzzle";
    private PuzzleSurfaceView view;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puzzle);

        getWindow().getDecorView().setBackgroundColor(Color.rgb(200,200,200));

        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        */

        /*
        PuzzleView view = findViewById(R.id.puzzle_view);
        view.setImage_src(new File(Objects.requireNonNull(getIntent().getStringExtra(IMAGE_FILE_PUZZLE))));
        view.reDraw();
        */

        MobileAds.initialize(this, getResources().getString(R.string.ad_application_id));
        AdView mAdView = findViewById(R.id.adView);
        if(WITH_AD_BANNER){
            mAdView.loadAd( new AdRequest.Builder().build() );
        }else{
            mAdView.setVisibility(View.GONE);
        }


        /*
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.ad_interstitial_1));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }
        */


        view = findViewById(R.id.puzzle_view);
        view.setImageFile(new File(Objects.requireNonNull(getIntent().getStringExtra(IMAGE_FILE_PUZZLE))));

        FloatingActionButton open_image = findViewById(R.id.open_image);
        open_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ii = new Intent(PuzzleActivity.this,ShowImageActivity.class);
                ii.putExtra(IMAGE_FILE_PUZZLE,getIntent().getStringExtra(IMAGE_FILE_PUZZLE));
                startActivity(ii);
            }
        });
    }
    @Override
    protected void onResume()
    {
        super.onResume();
        // start the drawing
        view.startDrawThread();
    }

    @Override
    protected void onPause()
    {
        // stop the drawing to save cpu time
        view.stopDrawThread();
        super.onPause();
    }
}
