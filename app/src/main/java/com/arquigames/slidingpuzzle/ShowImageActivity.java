package com.arquigames.slidingpuzzle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.io.File;
import java.util.Objects;

import static com.arquigames.slidingpuzzle.NavActivity.WITH_AD_BANNER;
import static com.arquigames.slidingpuzzle.PuzzleActivity.IMAGE_FILE_PUZZLE;

public class ShowImageActivity extends AppCompatActivity {

    private GestureDetector mGestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_image);
        File f = new File(Objects.requireNonNull(getIntent().getStringExtra(IMAGE_FILE_PUZZLE)));
        AppCompatImageView img = findViewById(R.id.show_image);
        img.setImageDrawable(BitmapDrawable.createFromPath(f.getAbsolutePath()));


        MobileAds.initialize(this, getResources().getString(R.string.ad_application_id));
        AdView mAdView = findViewById(R.id.adView);
        if(WITH_AD_BANNER){
            mAdView.loadAd( new AdRequest.Builder().build() );
        }else{
            mAdView.setVisibility(View.GONE);
        }



        mGestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onDown(MotionEvent motionEvent) {
                return true;
            }
            @Override
            public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
                return super.onFling(motionEvent,motionEvent1,v,v1);
            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        // TouchEvent dispatcher.
        if (mGestureDetector != null) {
            if (mGestureDetector.onTouchEvent(ev))
                // If the gestureDetector handles the event, a swipe has been
                // executed and no more needs to be done.
                return true;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mGestureDetector.onTouchEvent(event);
    }
}
