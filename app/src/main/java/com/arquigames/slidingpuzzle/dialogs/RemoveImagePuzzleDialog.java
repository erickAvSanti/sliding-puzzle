package com.arquigames.slidingpuzzle.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.arquigames.slidingpuzzle.NavActivity;
import com.arquigames.slidingpuzzle.R;
import com.labo.kaji.swipeawaydialog.support.v4.SwipeAwayDialogFragment;

public class RemoveImagePuzzleDialog extends SwipeAwayDialogFragment {

    private final int image_puzzle_position;
    NoticeDialogListener listener;

    public RemoveImagePuzzleDialog(int position) {
        image_puzzle_position = position;
    }

    @Override
    public void onResume(){
        super.onResume();

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.remove_image_puzzle_msg)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(listener != null) listener.onDialogPositiveClickRemoveImagePuzzle(RemoveImagePuzzleDialog.this,image_puzzle_position);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(listener != null) listener.onDialogNegativeClickRemoveImagePuzzle(RemoveImagePuzzleDialog.this,image_puzzle_position);
                    }
                });
        // Create the AlertDialog object and return it
        Dialog dialog = builder.create();


        return dialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            listener = (NoticeDialogListener) context;
        } catch (ClassCastException e) {
            NavActivity.printStackTrace(e);
        }
    }

    public interface NoticeDialogListener {
        public void onDialogPositiveClickRemoveImagePuzzle(RemoveImagePuzzleDialog dialog,int position);
        public void onDialogNegativeClickRemoveImagePuzzle(RemoveImagePuzzleDialog dialog,int position);
    }
}
