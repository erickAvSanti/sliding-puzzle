package com.arquigames.slidingpuzzle.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.arquigames.slidingpuzzle.NavActivity;
import com.arquigames.slidingpuzzle.PuzzleActivity;
import com.arquigames.slidingpuzzle.R;
import com.arquigames.slidingpuzzle.drawables.CustomDrawable;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SimpleItemRecyclerViewAdapter
        extends RecyclerView.Adapter<ViewHolder> {

    private final NavActivity ctx;
    public final List<CustomDrawable> mValues;

    public SimpleItemRecyclerViewAdapter(NavActivity ctx,
                                  List<CustomDrawable> items) {
        this.ctx = ctx;
        mValues = items;
    }
    public ArrayList<File> getDrawablesFiles(){
        ArrayList<File> files = new ArrayList<>();
        if(mValues.size()>0){
            for(CustomDrawable cd : mValues){
                files.add(cd.origin);
            }
        }
        return files;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_crop_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final CustomDrawable dw = mValues.get(position);
        holder.origin = dw.origin;
        holder.imageView.setImageDrawable(dw.drawable);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ctx, PuzzleActivity.class);
                i.putExtra(PuzzleActivity.IMAGE_FILE_PUZZLE,dw.origin.getAbsolutePath());
                ctx.showAd(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void removeItem(int position) {
        mValues.remove(position);
        notifyItemRemoved(position);
    }
}