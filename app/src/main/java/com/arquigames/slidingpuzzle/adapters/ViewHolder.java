package com.arquigames.slidingpuzzle.adapters;


import android.view.View;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.arquigames.slidingpuzzle.R;

import java.io.File;

public class ViewHolder extends RecyclerView.ViewHolder {
    public ImageView imageView;
    public File origin;
    ViewHolder(View view) {
        super(view);
        imageView = ((ImageView)view.findViewById(R.id.image_view));
    }
}