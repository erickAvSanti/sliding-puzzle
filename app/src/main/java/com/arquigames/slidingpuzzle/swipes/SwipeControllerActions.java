package com.arquigames.slidingpuzzle.swipes;


public abstract class SwipeControllerActions {

    public void onLeftClicked(int position) {}

    public void onRightClicked(int position) {}

}