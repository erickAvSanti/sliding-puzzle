package com.arquigames.slidingpuzzle.swipes;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;

import com.arquigames.slidingpuzzle.R;
import com.arquigames.slidingpuzzle.utils.math.Vector2d;
import java.util.Random;

public class PuzzleSwipeManager2 {
    private static final String TAG = "PuzzleSwipeManager";
    private int width = 0;
    private int height= 0;

    private int size = 0;

    private int dim = 4;

    private PuzzleBox[][] boxes = new PuzzleBox[dim][dim];
    private PuzzleBox[] boxes_indices = new PuzzleBox[dim*dim];

    private Bitmap image;
    private Bitmap[][] imgs = null;


    private int step;

    private Random rand = new Random();

    private RadialGradient radialGradient;
    private Paint paintRadialGradient;

    private boolean win = false;

    private View view;

    private int startX, startY;
    private Paint winPaint = new Paint();

    public PuzzleSwipeManager2(View view,int measuredWidth, int measuredHeight, Bitmap image) {
        this.width = measuredWidth;
        this.height = measuredHeight;
        this.image = image;
        this.view = view;

        if(image != null)Log.e(TAG,"Image is not null");
        else Log.e(TAG,"Image is null");

        size = width > height ? height : width;
        split();
        setProperties();
    }

    private void setProperties() {
        radialGradient = new RadialGradient(width/2, height/2, (int)(1.2*( width > height ? height : width )), 0xFF3693DB,
                0xFF000000, android.graphics.Shader.TileMode.CLAMP);

        paintRadialGradient = new Paint();

        winPaint.setStrokeWidth(2);
        winPaint.setStyle(Paint.Style.STROKE);
        winPaint.setColor(Color.rgb(0,255,0));
        winPaint.setTextSize(80);
    }

    private void split() {
        this.splitImage();
        step = size / dim;
        Log.e(TAG,"Size = "+size);
        Log.e(TAG,"Step = "+step);
        startX = ( width - size ) / 2;
        startY = ( height- size ) / 2;
        int sY = startY, sX;
        int id = 1;
        for( int j = 0; j < dim ; j++){
            sX = startX;
            for( int i = 0; i < dim ; i++){
                PuzzleBox box = boxes[j][i] = new PuzzleBox(id,sX, sY,step,step, (imgs != null ? imgs[j][i] : null),j,i);
                boxes_indices[id - 1] = box;
                sX += step;
                id ++;
            }
            sY += step;
        }
        randomizeBoxesPosition();
    }
    private void printBoxesProperties(PuzzleBox[] boxes){
        for(int i = 0 ; i < boxes.length ; i++){
            PuzzleBox box = boxes[i];
            Log.e(TAG,"box ID = "+box.id+", posX = "+box.posX+", posY = "+box.posY+", idxI = "+box.idxI+", idxJ = "+box.idxJ);
        }
    }
    private void randomizeBoxesPosition(){
        int[] new_pos = new int[]{3,13,8,2,7,12,6,9,14,5,10,15,1,4,11,0};
        PuzzleBox[] tmp_boxes = new PuzzleBox[dim*dim];
        for( PuzzleBox box : boxes_indices){
            tmp_boxes[box.id-1] = box.clone();
        }
        printBoxesProperties(boxes_indices);
        for(int index = 0 ; index < new_pos.length ; index ++ ){

            PuzzleBox tmp_box   = tmp_boxes[new_pos[index]];
            PuzzleBox box       = boxes_indices[index];
            box.copyPos(tmp_box);
            box.copyIdx(tmp_box);
            //tmp_box.image       = box.image;
            //switchBox(box,tmp_box);
        }
        //boxes_indices = tmp_boxes;
        for(PuzzleBox box : boxes_indices){
            boxes[box.idxJ][box.idxI] = box;
        }
        printBoxesProperties(boxes_indices);
        boxes_indices[(int)(boxes_indices.length * rand.nextFloat())].isBlank = true;

    }
    private void splitImage(){
        if(image == null) return;
        imgs = new Bitmap[dim][dim];
        int image_width = image.getWidth();
        int image_height= image.getHeight();
        Log.e(TAG,"Image widht = "+image_width);
        Log.e(TAG,"Image height = "+image_height);
        int chunkWidth = image_width / dim;
        int chunkHeight = image_height/ dim;
        for( int j = 0; j < dim ; j++){
            for( int i = 0; i < dim ; i++){
                Bitmap img = imgs[j][i] = Bitmap.createBitmap(image, i * chunkWidth, j * chunkHeight, chunkWidth, chunkHeight);
                Log.e(TAG,"Split image for pos : i= "+i+", j="+j+", chunk_width = "+img.getWidth()+", chunk_height = "+img.getHeight());
            }
        }
    }
    private void printBoxesLimits(){
        for( int j = 0; j < dim ; j++){
            for( int i = 0; i < dim ; i++){
                //System.out.println(boxes[j][i].toString());
                Log.e(TAG,"box = "+boxes[j][i].toString());
            }
        }
    }

    public void draw(Canvas canvas) {
        //printBoxesLimits();
        drawGradientCircle(canvas);
        for( int j = 0; j < dim ; j++){
            for( int i = 0; i < dim ; i++){
                PuzzleBox box = boxes[j][i];
                if(box.isBlank){
                    box.draw(canvas,null,false);
                }else{
                    box.draw(canvas,null,true);
                }
            }
        }
        if(win)drawWinText(canvas);
    }

    private void drawWinText(Canvas canvas) {
        String text = view.getContext().getResources().getString(R.string.win_text);

        canvas.drawText(text,width/2 - winPaint.measureText(text) / 2,startY - 40,winPaint);
    }

    private PuzzleBox getBoxFor(int x, int y){
        for( int j = 0; j < dim ; j++){
            for( int i = 0; i < dim ; i++){
                PuzzleBox box = boxes[j][i];
                if(box.containsPoint(x,y)){
                    return box;
                }
            }
        }
        return null;
    }

    public void onTouchEnd(float x, float y) {
        PuzzleBox box = getBoxFor((int)x,(int)y),tmpBox;
        int tmpI,tmpJ;
        boolean switched = false;
        if(box != null){
            tmpJ = box.idxJ;
            tmpI = box.idxI - 1;

            if(tmpJ>=0 && tmpI >=0 && tmpJ < dim && tmpI < dim){
                tmpBox = boxes[tmpJ][tmpI];
                if( tmpBox.isBlank ){
                    switchBox2(tmpBox,box);
                    switched = true;
                }
            }

            tmpI = box.idxI + 1;
            if(!switched && tmpJ>=0 && tmpI >=0 && tmpJ < dim && tmpI < dim){
                tmpBox = boxes[tmpJ][tmpI];
                if( tmpBox.isBlank ){
                    switchBox2(tmpBox,box);
                    switched = true;
                }
            }

            tmpI = box.idxI;
            tmpJ = box.idxJ - 1;
            if(!switched && tmpJ>=0 && tmpI >=0 && tmpJ < dim && tmpI < dim){
                tmpBox = boxes[tmpJ][tmpI];
                if( tmpBox.isBlank ){
                    switchBox2(tmpBox,box);
                    switched = true;
                }
            }
            tmpJ = box.idxJ + 1;
            if(!switched && tmpJ>=0 && tmpI >=0 && tmpJ < dim && tmpI < dim){
                tmpBox = boxes[tmpJ][tmpI];
                if( tmpBox.isBlank ){
                    switchBox2(tmpBox,box);
                }
            }
        }
        if(!win && validate()){
            win = true;
        }
    }
    private void switchBox2(PuzzleBox box1, PuzzleBox box2){

        int tmpIdxI = box1.idxI;
        int tmpIdxJ = box1.idxJ;

        int tmpX = box1.posX;
        int tmpY = box1.posY;

        box1.idxI = box2.idxI;
        box1.idxJ = box2.idxJ;

        box1.posX = box2.posX;
        box1.posY = box2.posY;

        box2.idxI = tmpIdxI;
        box2.idxJ = tmpIdxJ;

        box2.posX = tmpX;
        box2.posY = tmpY;

        boxes[box1.idxJ][box1.idxI] = box1;
        boxes[box2.idxJ][box2.idxI] = box2;

    }
    private boolean validate(){
        int index = 1;
        for( int j = 0; j < dim ; j++){
            for( int i = 0; i < dim ; i++){
                PuzzleBox box = boxes[j][i];
                if(box.id != index){
                    return false;
                }
                index ++;
            }
        }
        return true;
    }
    private void switchBox(PuzzleBox box1, PuzzleBox box2) {

        int tmpIdxI = box1.idxI;
        int tmpIdxJ = box1.idxJ;

        int tmpX = box1.posX;
        int tmpY = box1.posY;

        box1.idxI = box2.idxI;
        box1.idxJ = box2.idxJ;

        box1.posX = box2.posX;
        box1.posY = box2.posY;

        box2.idxI = tmpIdxI;
        box2.idxJ = tmpIdxJ;

        box2.posX = tmpX;
        box2.posY = tmpY;

        boxes[box1.idxJ][box1.idxI] = box1;
        boxes[box2.idxJ][box2.idxI] = box2;
    }

    private void drawGradientCircle(Canvas canvas){
        paintRadialGradient.setDither(true);
        paintRadialGradient.setShader(radialGradient);
        canvas.drawPaint(paintRadialGradient);
        //canvas.drawCircle(width/2,height/2,(width > height ? height : width) / 2, paintRadialGradient);
    }

    public void resize(int width, int height) {
        size = width > height ? height : width;
        step = size / dim;
        Log.e(TAG,"Size = "+size);
        Log.e(TAG,"Step = "+step);
        startX = ( width - size ) / 2;
        startY = ( height- size ) / 2;
        int sY = startY, sX;
        int id = 1;
        for( int j = 0; j < dim ; j++){
            sX = startX;
            for( int i = 0; i < dim ; i++){
                PuzzleBox box = boxes[j][i];
                box.posX = sX;
                box.posY = sY;
                box.width = step;
                box.height = step;
                /*
                PuzzleBox box = boxes[j][i] = new PuzzleBox(id,sX, sY,step,step, (imgs != null ? imgs[j][i] : null),j,i);
                boxes_indices[id - 1] = box;

                 */
                sX += step;
                id ++;
            }
            sY += step;
        }


        this.width = width;
        this.height = height;
    }

    public class PuzzleBox {
        public int posX, posY, width, height,id;
        public Bitmap image;
        public boolean isBlank;
        private Paint paint = new Paint();
        private Paint paintLine = new Paint();
        private Rect image_rect_src = new Rect();
        private Rect image_rect_dst = new Rect();
        private int idxJ,idxI;
        private Paint paintText = new Paint();

        PuzzleBox(int id, int posX, int posY, int width, int height, Bitmap image, int idxJ,int idxI){
            this.id = id;
            this.posX = posX;
            this.posY = posY;
            this.width = width;
            this.height = height;
            this.image = image;
            this.idxJ = idxJ;
            this.idxI = idxI;
            setPaints();
            setRectSrc();
        }
        public PuzzleBox copy(PuzzleBox box){
            this.id = box.id;
            this.posX = box.posX;
            this.posY = box.posY;
            this.width = box.width;
            this.height = box.height;
            this.image = box.image;
            this.idxJ = box.idxJ;
            this.idxI = box.idxI;
            setRectSrc();
            return this;
        }
        public PuzzleBox clone(){
            return new PuzzleBox(this.id,this.posX,this.posY,this.width,this.height,this.image,this.idxJ,this.idxI);
        }
        private void setPaints(){
            paintLine.setStyle(Paint.Style.STROKE);
            paintLine.setStrokeWidth(5);
            paintLine.setColor(Color.rgb(179,203,44));
            paintLine.setAlpha(255);

            paintText.setStyle(Paint.Style.STROKE);
            paintText.setStrokeWidth(3);
            paintText.setTextSize(50);
            paintText.setColor(Color.rgb(0,0,0));
        }
        public void setRects(){
            setRectSrc();
            setRectDst(0,0);
        }
        private void setRectSrc(){

            image_rect_src.top = 0;
            image_rect_src.left = 0;
            image_rect_src.right = this.image.getWidth();
            image_rect_src.bottom= this.image.getHeight();
        }
        private void setRectDst(int x, int y){
            image_rect_dst.top = this.posY + y;
            image_rect_dst.left = this.posX + x;
            image_rect_dst.right = image_rect_dst.left + this.width;
            image_rect_dst.bottom = image_rect_dst.top + this.height;
        }

        public boolean containsPoint(int x, int y){
            return ( x > this.posX && x < this.posX + this.width ) && ( y > this.posY && y < this.posY + this.height );
        }
        @NonNull
        public String toString(){
            return String.format("posX=%s, posY=%s, width=%s, height=%s",this.posX,this.posY,this.width,this.height);
        }

        public void draw(Canvas canvas, Vector2d tmp,boolean drawImage) {
            if(tmp != null){
                this.setRectDst((int)tmp.x,(int)tmp.y);
            }else{
                this.setRectDst(0,0);
            }
            if(drawImage){
                canvas.drawBitmap(this.image,this.image_rect_src,this.image_rect_dst,this.paint);
            }
            canvas.drawRect(image_rect_dst,paintLine);
            //canvas.drawText(String.valueOf(id),posX+paintText.getTextSize(),posY+paintText.getTextSize(),paintText);
        }

        public void copyPos(PuzzleBox tmp_box) {
            posX = tmp_box.posX;
            posY = tmp_box.posY;
        }

        public void copyIdx(PuzzleBox tmp_box) {
            idxI = tmp_box.idxI;
            idxJ = tmp_box.idxJ;
        }
    }
    public static void main(String[] a){
        System.out.println(a);
        PuzzleSwipeManager2 p = new PuzzleSwipeManager2(null,1000,3000,null);
        p.printBoxesLimits();
    }
}
