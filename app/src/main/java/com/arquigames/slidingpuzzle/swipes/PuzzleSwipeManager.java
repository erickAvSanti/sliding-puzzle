package com.arquigames.slidingpuzzle.swipes;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

import androidx.annotation.NonNull;

import com.arquigames.slidingpuzzle.utils.math.Vector2d;
import com.arquigames.slidingpuzzle.views.PuzzleView;

public class PuzzleSwipeManager {
    private static final String TAG = "PuzzleSwipeManager";
    private int width = 0;
    private int height= 0;

    private int size = 0;

    private int dim = 4;

    private PuzzleBox[][] boxes = new PuzzleBox[dim][dim];
    private PuzzleBox[] boxes_indices = new PuzzleBox[dim*dim];

    private Bitmap image;
    private Bitmap[][] imgs = null;

    private PuzzleView puzzleView;

    private PuzzleBox boxTouched;

    private Vector2d startPoint;
    private Vector2d endPoint;

    private int step;

    public PuzzleSwipeManager(PuzzleView view,int measuredWidth, int measuredHeight, Bitmap image) {
        this.width = measuredWidth;
        this.height = measuredHeight;
        this.image = image;
        this.puzzleView = view;

        size = width > height ? height : width;
        split();
    }

    private void split() {
        this.splitImage();
        step = size / dim;
        int startX = ( width - size ) / 2;
        int startY = ( height- size ) / 2;
        int sY = startY, sX;
        int id = 1;
        for( int j = 0; j < dim ; j++){
            sX = startX;
            for( int i = 0; i < dim ; i++){
                PuzzleBox box = boxes[j][i] = new PuzzleBox(id,sX, sY,step,step, (imgs != null ? imgs[j][i] : null),j,i);
                boxes_indices[id - 1] = box;
                sX += step;
                id ++;
            }
            sY += step;
        }
        randomizeBoxesPosition();
    }
    private void randomizeBoxesPosition(){
        int[] new_pos = new int[]{3,13,8,2,7,12,6,9,14,5,10,15,1,4,11,0};
        PuzzleBox[] tmp_boxes = new PuzzleBox[dim*dim];
        for( PuzzleBox box : boxes_indices){
            tmp_boxes[box.id-1] = box.clone();
        }
        for(int index = 0 ; index < new_pos.length ; index ++ ){

            PuzzleBox tmp_box   = tmp_boxes[index];
            PuzzleBox box       = boxes_indices[new_pos[index]];
            tmp_box.image       = box.image;

        }
        boxes_indices = tmp_boxes;
        for(PuzzleBox box : boxes_indices){
            boxes[box.idxJ][box.idxI] = box;
        }

    }
    private void splitImage(){
        if(image == null) return;
        imgs = new Bitmap[dim][dim];
        int image_width = image.getWidth();
        int image_height= image.getHeight();
        int chunkWidth = image_width / dim;
        int chunkHeight = image_height/ dim;
        for( int j = 0; j < dim ; j++){
            for( int i = 0; i < dim ; i++){
                imgs[j][i] = Bitmap.createBitmap(image, i * chunkWidth, j * chunkHeight, chunkWidth, chunkHeight);
            }
        }
    }
    private void printBoxesLimits(){
        for( int j = 0; j < dim ; j++){
            for( int i = 0; i < dim ; i++){
                //System.out.println(boxes[j][i].toString());
                Log.e(TAG,"box = "+boxes[j][i].toString());
            }
        }
    }

    public void draw(Canvas canvas) {
        //printBoxesLimits();
        for( int j = 0; j < dim ; j++){
            for( int i = 0; i < dim ; i++){
                PuzzleBox box = boxes[j][i];
                if( boxTouched != null && boxTouched.id == box.id)continue;
                box.draw(canvas,null);
            }
        }
        moveCurrentBoxTouched(canvas);
    }
    private PuzzleBox getBoxFor(int x, int y){
        for( int j = 0; j < dim ; j++){
            for( int i = 0; i < dim ; i++){
                PuzzleBox box = boxes[j][i];
                if(box.containsPoint(x,y)){
                    return box;
                }
            }
        }
        return null;
    }
    private void moveCurrentBoxTouched(Canvas canvas) {
        if(boxTouched != null && endPoint != null && startPoint != null ){
            Vector2d tmp = endPoint.clone();
            tmp.sub(startPoint);
            if(Math.abs(tmp.x) > Math.abs(tmp.y) ){
                tmp.y = 0;
            }else{
                tmp.x = 0;
            }
            tmp.maxLength(step);
            boxTouched.draw(canvas,tmp);
        }
    }

    public void onTouchStart(float x, float y) {
        startPoint = new Vector2d(x,y);
        PuzzleBox box = getBoxFor((int)x,(int)y);
        if(box !=null){
            boxTouched = box;
        }
    }
    public void onTouchMove(float x, float y) {
        endPoint = new Vector2d(x,y);
    }


    public void onTouchEnd(float x, float y) {
        dropCurrentBoxTouched(x,y);
        startPoint = null;
        endPoint = null;
        boxTouched = null;
    }

    private void dropCurrentBoxTouched(float x, float y) {
        if(startPoint == null) return;
        Vector2d tmp = new Vector2d(x,y);
        tmp.sub(startPoint);
        if( Math.abs(tmp.x) > Math.abs(tmp.y)){
            tmp.y = 0;
        }else{
            tmp.x = 0;
        }
        tmp.maxLength(step);
        tmp.add(startPoint);
        PuzzleBox tmpBox = getBoxFor((int)tmp.x,(int)tmp.y);
        if( tmpBox != null && tmpBox.id != boxTouched.id ){
            int tmpIdxI = tmpBox.idxI;
            int tmpIdxJ = tmpBox.idxJ;

            int tmpX = tmpBox.posX;
            int tmpY = tmpBox.posY;

            tmpBox.idxI = boxTouched.idxI;
            tmpBox.idxJ = boxTouched.idxJ;

            tmpBox.posX = boxTouched.posX;
            tmpBox.posY = boxTouched.posY;

            boxTouched.idxI = tmpIdxI;
            boxTouched.idxJ = tmpIdxJ;

            boxTouched.posX = tmpX;
            boxTouched.posY = tmpY;

            boxes[tmpBox.idxJ][tmpBox.idxI] = tmpBox;
            boxes[boxTouched.idxJ][boxTouched.idxI] = boxTouched;

        }
    }

    public class PuzzleBox {
        public int posX, posY, width, height,id;
        public Bitmap image;
        private Paint paint = new Paint();
        private Paint paintLine = new Paint();
        private Rect image_rect_src = new Rect();
        private Rect image_rect_dst = new Rect();
        private int idxJ,idxI;
        PuzzleBox(int id, int posX, int posY, int width, int height, Bitmap image, int idxJ,int idxI){
            this.id = id;
            this.posX = posX;
            this.posY = posY;
            this.width = width;
            this.height = height;
            this.image = image;
            this.idxJ = idxJ;
            this.idxI = idxI;
            setPaints();
            setRectSrc();
        }
        public PuzzleBox copy(PuzzleBox box){
            this.id = box.id;
            this.posX = box.posX;
            this.posY = box.posY;
            this.width = box.width;
            this.height = box.height;
            this.image = box.image;
            this.idxJ = box.idxJ;
            this.idxI = box.idxI;
            setRectSrc();
            return this;
        }
        public PuzzleBox clone(){
            return new PuzzleBox(id,posX,posY,width,height,image,idxJ,idxI);
        }
        private void setPaints(){
            paintLine.setAlpha(255);
            paintLine.setStyle(Paint.Style.STROKE);
            paintLine.setStrokeWidth(10);
            paintLine.setColor(Color.RED);
        }
        public void setRects(){
            setRectSrc();
            setRectDst(0,0);
        }
        private void setRectSrc(){

            this.image_rect_src.top = 0;
            this.image_rect_src.left = 0;
            this.image_rect_src.right = image.getWidth();
            this.image_rect_src.bottom= image.getHeight();
        }
        private void setRectDst(int x, int y){
            this.image_rect_dst.top = this.posY + y;
            this.image_rect_dst.left = this.posX + x;
            this.image_rect_dst.right = this.image_rect_dst.left + this.width;
            this.image_rect_dst.bottom = this.image_rect_dst.top + this.height;
        }

        public boolean containsPoint(int x, int y){
            return ( x > this.posX && x < this.posX + this.width ) && ( y > this.posY && y < this.posY + this.height );
        }
        @NonNull
        public String toString(){
            return String.format("posX=%s, posY=%s, width=%s, height=%s",this.posX,this.posY,this.width,this.height);
        }

        public void draw(Canvas canvas, Vector2d tmp) {
            if(tmp != null){
                this.setRectDst((int)tmp.x,(int)tmp.y);
            }else{
                this.setRectDst(0,0);
            }
            canvas.drawBitmap(this.image,image_rect_src,image_rect_dst,paint);
            //canvas.drawLine(image_rect_dst.left,image_rect_dst.top,image_rect_dst.right,image_rect_dst.bottom,paintLine);
        }
    }
    public static void main(String[] a){
        System.out.println(a);
        PuzzleSwipeManager p = new PuzzleSwipeManager(null,1000,3000,null);
        p.printBoxesLimits();
    }
}
